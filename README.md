# indie-discover

> indie-discover is an indie web endpoint discovery module
>
> indie-discover, is part of the indie-pen-dent project, a set of
> javascript/nodejs applications which implement the indie web standards.

[![pipeline status](https://gitlab.com/indie-pen-dent/indie-discover/badges/master/pipeline.svg)](https://gitlab.com/indie-pen-dent/indie-discover/-/commits/master)
[![coverage report](https://gitlab.com/indie-pen-dent/indie-discover/badges/master/coverage.svg)](https://gitlab.com/indie-pen-dent/indie-discover/-/commits/master)
[![Latest Release](https://gitlab.com/indie-pen-dent/indie-discover/-/badges/release.svg)](https://gitlab.com/indie-pen-dent/indie-discover/-/releases)
[![semantic-release: angular](https://img.shields.io/badge/semantic--release-e10079?logo=semantic-release)](https://github.com/semantic-release/semantic-release)

## Installation

```sh
pnpm i @srchetwynd/indie-discover
```

Or:

```sh
npm i @srchetwynd/indie-discover
```

## Usage

The module exports a single class `EndpointDiscoverer`, which requires 2
parameters:
1. A url of the site to discover endpoints from
2. A HTML parser

### HTML parser
The html parser is function which will take a HTML page as a string, and return
an object with a `getHref` function which will retrieve the href from the
document.

Here is an example using Cheerio:
```typescript
import { load as cheerioLoad } from 'cheerio';

function HTMLParserFactory(page: string) {
    const loadedPage = cheerioLoad(page);
    const getHref = (query: string) => {
        const endpointElement = loadedPage(query);
        return endpointElement?.attr('href');
    };

    return {
        getHref,
    };
}
```

And an example using the browsers HTML parser:
```typescript
function HTMLParserFactory(page: string) {
    const loadedPage = document.createElement('template');
    const getHref = (query: string) => {
        const endpointElement = loadedPage.content.querySelector(query);
        return endpointElement?.attributes.getNamedItem('href')?.value;
    };
}
```

### Interface

The Endpoint Discoverer has 8 public methods:

```
discoverAuthorizationEndpoint(): Promise<string | undefined>
discoverTokenEndpoint(): Promise<string | undefined>
discoverRevocationEndpoint(): Promise<string | undefined> 
discoverIntrospectionEndpoint(): Promise<string | undefined> 
discoverUserinfoEndpoint(): Promise<string | undefined> 
discoverMicropubEndpoint(): Promise<string | undefined> 
discoverMicrosubEndpoint(): Promise<string | undefined> 
discoverMetadataEndpoint(): Promise<string | undefined> 
```

Each method will return the url of the endpoint as provided on the website at
the provided URL.

If the site does not have the requested endpoint `undefined` is returned.

## Support
The best place for support will be on the [gitlab repository](https://gitlab.com/indie-pen-dent/indie-discover/-/issues)

## Roadmap
Currently this supports:
- Endpoints:
  - Authorization
  - Token
  - Revocation
  - Introspection
  - Userinfo
  - Micropub
  - Microsub
  - Metadata
- Discovery through HTML elements


Still to support
- Discovery through `link` headers
- Support relative urls
- Living standard:
  - Support `indieauth-metadata` endpoint

## Contributing
I'm open to contributions, please submit issues in to the respository for ideas
to improve the project. All ideas are welcome and appreciated.

Any merge will be welcome and considered, but they must pass the current linting
rules, as well as the current tests, and ideally add more.

### Setting up for development
These steps will get a development setup running.

1. clone this repo,
3. run `pnpm i` (If installed, or checkout why its awesome here [https://pnpm.io/](https://pnpm.io/))
4. run `pnpm run dev`


## Authors and acknowledgment
It is currently a solo effort by [me](https://www.srchetwynd.co.uk), but any
help is welcome.

It would be unfair not to mention [indieauth-client-php](https://github.com/indieweb/indieauth-client-php)
projects, whilst this project is not a fork of it, it was heavily influenced
by it.

## License
This project is licensed under the [MIT License](https://mit-license.org/). A
copy of which should be included in this repository.

## Project status
This is currently under active development, but only in spare time around work,
and other commitments.
