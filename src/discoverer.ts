type PageSearcher = {
    getHref: (query: string) => string | undefined;
};

type PageSearcherFactory = (html: string) => PageSearcher;

export class EndpointDiscoverer {
    private me: string;
    private pageSearcherFactory: PageSearcherFactory;
    private _page: PageSearcher | undefined;
    private _headers: Headers | undefined;

    static fetch = fetch;

    constructor(me: string, pageSearcherFactory: PageSearcherFactory) {
        this.me = me;
        this.pageSearcherFactory = pageSearcherFactory;
    }

    private async loadPage() {
        const response = await EndpointDiscoverer.fetch(this.me);
        this._headers = response.headers;

        if (response.status !== 200) {
            throw new Error('"me" url returned a none 200 status.');
        }

        this._page = this.pageSearcherFactory(await response.text());
    }

    private async getPage() {
        if (!this._page) {
            await this.loadPage();
        }

        return this._page;
    }

    private async getHeaders() {
        if (!this._headers) {
            await this.loadPage();
        }

        return this._headers;
    }

    async discoverEndpoint(endpoint: string): Promise<string | undefined> {
        const headers = await this.getHeaders();

        if (headers === undefined) {
            throw new Error('This should be unreachable');
        }

        const headerValue = headers.get(endpoint);
        if (headerValue) {
            return headerValue;
        }

        const page = await this.getPage();

        if (page === undefined) {
            throw new Error('This should be unreachable');
        }

        const endpointHref = page.getHref(`[rel="${endpoint}"]`);

        if (!endpointHref) {
            return;
        }

        return endpointHref;
    }

    discoverAuthorizationEndpoint(): Promise<string | undefined> {
        return this.discoverEndpoint('authorization_endpoint');
    }

    discoverTokenEndpoint(): Promise<string | undefined> {
        return this.discoverEndpoint('token_endpoint');
    }

    discoverRevocationEndpoint(): Promise<string | undefined> {
        return this.discoverEndpoint('revocation_endpoint');
    }

    discoverIntrospectionEndpoint(): Promise<string | undefined> {
        return this.discoverEndpoint('introspection_endpoint');
    }

    discoverUserinfoEndpoint(): Promise<string | undefined> {
        return this.discoverEndpoint('userinfo_endpoint');
    }

    discoverMicropubEndpoint(): Promise<string | undefined> {
        return this.discoverEndpoint('micropub');
    }

    discoverMicrosubEndpoint(): Promise<string | undefined> {
        return this.discoverEndpoint('microsub');
    }

    discoverMetadataEndpoint(): Promise<string | undefined> {
        return this.discoverEndpoint('indieauth-metadata');
    }
}
